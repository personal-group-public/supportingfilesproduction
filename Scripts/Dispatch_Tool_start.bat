set productionName=dispatch_tool_release
git config --depth 1 --global user.email "deepak.singhal@vecmocon.com"
git clone https://gitlab.com/personal-group-public/production_logs.git
cd production_logs
git config user.name "%productionName%"_Elymer
git branch %productionName%
git push -u origin %productionName%
git checkout %productionName%
cd ..
start cmd.exe /c pushLog.bat
git clone --depth 1 https://gitlab.com/personal-group-public/%productionName%.git
cd %productionName%
git pull
cd main
main.exe