set productionName=production_default
set downloadSupportFiles=0

if %downloadSupportFiles%==1 (git clone --depth 1 https://gitlab.com/personal-group-public/supportingfilesproduction.git)
git clone --depth 1 https://gitlab.com/personal-group-public/%productionName%.git

cd %productionName%
git pull
start cmd.exe /c pushLog.bat  %productionName%
timeout 20
cd main
main.exe
timeout 30