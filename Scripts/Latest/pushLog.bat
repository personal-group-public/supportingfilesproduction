if not DEFINED IS_MINIMIZED set IS_MINIMIZED=1 && start "" /min "%~dpnx0" %* && exit
set productionName=%1

git config --global user.email "deepak.singhal@vecmocon.com"
git config user.name "%productionName%"

set "REPO_URL=https://gitlab.com/personal-group-public/production_logs.git"
set "BRANCH_NAME=%productionName%"
set "REMOTE_NAME=origin"

git clone %REPO_URL% ../production_logs
cd ../production_logs
git branch %productionName%
git push -u origin %productionName%
git checkout %productionName%
:loop
git add .
git commit -m logs_%TIME%
git push
timeout 3000 /nobreak
goto loop
exit